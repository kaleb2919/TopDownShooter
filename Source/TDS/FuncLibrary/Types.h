// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/Image.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"

#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
    Aim_State UMETA(DisplayName = "Aim State"),
    AimWalk_State UMETA(DisplayName = "AimWalk State"),
    Walk_State UMETA(DisplayName = "Walk State"),
    Run_State UMETA(DisplayName = "Run State"),
    SprintRun_State UMETA(DisplayName = "SprintRun State")
};

UENUM(BlueprintType)
enum class EProjectileType : uint8
{
    Projectile UMETA(DisplayName = "Projectile"),
    Trace UMETA(DisplayName = "Trace"),
};

UENUM(BlueprintType)
enum class EItemType : uint8
{
    Ammo UMETA(DisplayName = "Ammo"),
    Weapon UMETA(DisplayName = "Weapon"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimSpeedNormal = 300.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float WalkSpeedNormal = 200.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float RunSpeedNormal = 600.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float AimSpeedWalk = 100.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    float SprintRunSpeedRun = 800.0f;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
    FName Name = TEXT("Default Item");
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
    int Count = 20;

    bool operator == (const FAmmoSlot& Other) const;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
    FName Name = TEXT("Default");
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
    int AmmoInClip = 20;

    bool operator == (const FWeaponSlot& Other) const;
};

USTRUCT(BlueprintType)
struct FSpawnStaticMeshParam
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
        UStaticMesh* Mesh = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
        float Impulse = 5.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
        float LifeTime = 10.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
        float Scale = 1.f;
};

USTRUCT(BlueprintType)
struct FProjectileParams
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Type")
    EProjectileType ProjectileType = EProjectileType::Projectile;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    TSubclassOf<class AProjectileDefault> ProjectileClass = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    float LifeTime = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    float InitSpeed = 2000.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
    float LengthTrace = 1000.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
    float Damage = 20.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage ")
    float DamageRadius = 0.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage ")
    UCurveFloat* DamageRadiusFalloffCurve = nullptr;


    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    USoundBase* HitSound = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    UParticleSystem* ExplosionFX = nullptr;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float AimDispersion = .3f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float AimWalkDispersion = 0.4f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float WalkDispersion = 0.2f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    float RunDispersion = 0.1f;
};

USTRUCT(BlueprintType)
struct FWeaponParams : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
    TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
    UStaticMesh* PickupMesh = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    USoundBase* PickupSound = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Particle")
    UParticleSystem* PickupParticle = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
    FSpawnStaticMeshParam MagazineDrop;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
    FSpawnStaticMeshParam ShellDrop;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire")
    float FireRate = 0.5f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire")
    float ReloadRate = 2.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire")
    int32 NumberRound = 10;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire")
    int32 NumberProjectileByShot = 1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
    FWeaponDispersion DispersionWeapon;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    USoundBase* SoundFireWeapon = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    USoundBase* SoundReloadWeapon = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    UParticleSystem* EffectFireWeapon = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
    FProjectileParams ProjectileParams;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    UAnimMontage* AnimCharFire = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    UAnimMontage* AnimCharAimFire = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    UAnimMontage* AnimCharReload = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    UAnimMontage* AnimWeaponFire = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
    UAnimMontage* AnimWeaponReload = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
    FName AmmoType;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
    UTexture* Icon = nullptr;
};

USTRUCT(BlueprintType)
struct FAmmoParams : public FTableRowBase
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
    UStaticMesh* PickupMesh = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
    USoundBase* PickupSound = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Particle")
    UParticleSystem* PickupParticle = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
    int MaxCount = 100;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Common")
    UTexture* Icon = nullptr;
};

USTRUCT(BlueprintType)
struct FCharacterMovementState
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        bool SlowWalk = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        bool Aim = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        bool Walk = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
        bool Run = false;
};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
      GENERATED_BODY()
};
