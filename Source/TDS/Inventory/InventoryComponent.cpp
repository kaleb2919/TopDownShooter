// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryComponent.h"

UInventoryComponent::UInventoryComponent()
{
    Weapons.Init(nullptr, MaxWeapon);
}

FAmmoSlot* UInventoryComponent::FindAmmoByName(FName AmmoName)
{
    for (FAmmoSlot* CurrentItemSlot: Items)
    {
        if (CurrentItemSlot->Name == AmmoName)
        {
            return CurrentItemSlot;
        }
    }

    return nullptr;
}

FWeaponSlot* UInventoryComponent::FindWeaponByName(FName WeaponName)
{
    for (FWeaponSlot* CurrentWeaponSlot: Weapons)
    {
        if (CurrentWeaponSlot)
        {
            if (CurrentWeaponSlot->Name == WeaponName)
            {
                return CurrentWeaponSlot;
            }
        }
    }

    return nullptr;
}

bool UInventoryComponent::AddAmmo(FName AmmoName, int Count, int MaxCount)
{
    if (!AmmoName.IsValid())
    {
        UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::AddAmmo - not valid"));
        return false;
    }

    bool bResult = true;
    FAmmoSlot* ItemSlot = FindAmmoByName(AmmoName);

    if (ItemSlot)
    {
        if (ItemSlot->Count == MaxCount && Count >= 0)
        {
            bResult = false;
        }
        else
        {
            ItemSlot->Count = FMath::Clamp<int>(ItemSlot->Count + Count, 0, MaxCount);
            OnAmmoChanged.Broadcast(AmmoName, ItemSlot->Count);
        }
    }
    else
    {
        Items.Add(new FAmmoSlot {AmmoName, Count <= MaxCount ? Count : MaxCount});
        OnAmmoChanged.Broadcast(AmmoName, Count);
    }

    UE_LOG(LogTemp, Log, TEXT("UInventoryComponent::AddAmmo - Name = %s, Count = %d, MaxCount = %d"), *AmmoName.ToString(), Count, MaxCount);

    return bResult;
}

FWeaponSlot UInventoryComponent::GetWeapon(const int Slot)
{
    return *Weapons[Slot];
}

bool UInventoryComponent::AddWeapon(FName WeaponName, const int AmmoInClip)
{
    if (!WeaponName.IsValid())
    {
        UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::AddWeapon - not valid"));
        return false;
    }

    bool bResult = true;

    if (FindWeaponByName(WeaponName))
    {
        bResult = false;
    }
    else
    {
        for (int i = 0; i < MaxWeapon; i++)
        {
            if (Weapons[i] == nullptr)
            {
                Weapons[i] = new FWeaponSlot {WeaponName, AmmoInClip};
                OnWeaponSlotChanged.Broadcast(WeaponName, i);
                break;
            }
        }
    }

    UE_LOG(LogTemp, Log, TEXT("UInventoryComponent::AddWeapon - Name = %s, AmmoInClip = %d"), *WeaponName.ToString(), AmmoInClip);

    return bResult;
}

int UInventoryComponent::GetAvailableAmmo(FName AmmoName, const int NeedCount)
{
    if (!AmmoName.IsValid())
    {
        UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::GetAvailableAmmo - not valid"));
        return 0;
    }

    int total = 0;
    FAmmoSlot* ItemSlot = FindAmmoByName(AmmoName);

    if (ItemSlot)
    {
        if (ItemSlot->Count >= NeedCount)
        {
            total = NeedCount;
        }
        else
        {
            total = ItemSlot->Count;
        }
    }

    return total;
}

int UInventoryComponent::CountAmmo(FName AmmoName)
{
    if (!AmmoName.IsValid())
    {
        UE_LOG(LogTemp, Warning, TEXT("UInventoryComponent::CountAmmo - not valid"));
        return 0;
    }

    int total = 0;
    FAmmoSlot* ItemSlot = FindAmmoByName(AmmoName);

    if (ItemSlot)
    {
        total =  ItemSlot->Count;
    }

    UE_LOG(LogTemp, Log, TEXT("UInventoryComponent::CountAmmo - total = %d"), total);

    return total;
}

int UInventoryComponent::CountWeapons()
{
    int total = 0;
    for (auto Weapon : Weapons)
    {
        if (Weapon)
        {
            total++;
        }
    }

    return  total;
}

bool UInventoryComponent::IsValidWeaponSlot(int Slot)
{
    if (Weapons.IsValidIndex(Slot))
    {
        return Weapons[Slot] != nullptr;
    }

    return false;
}

void UInventoryComponent::SetAmmoInClip(int Slot, int Count)
{
    UE_LOG(LogTemp, Log, TEXT("UInventoryComponent::SetAmmoInClip - Slot = %d, Count = %d"), Slot, Count);

    Weapons[Slot]->AmmoInClip = Count;
}

TArray<FAmmoSlot> UInventoryComponent::GetAmmoSlots()
{
    TArray<FAmmoSlot> array;
    for (auto AmmoSlot : Items)
    {
        array.Add(*AmmoSlot);
    }

    return array;
}

FAmmoSlot UInventoryComponent::GetAmmoSlot(int Slot)
{
    if (Items[Slot])
    {
        return  *Items[Slot];
    }

    return FAmmoSlot();
}

TArray<FWeaponSlot> UInventoryComponent::GetWeaponSlots()
{
    TArray<FWeaponSlot> WeaponSlots;
    WeaponSlots.Init(FWeaponSlot(), 5);

    for (int i = 0; i < MaxWeapon; i++)
    {
        if (Weapons[i])
        {
            WeaponSlots[i] = *Weapons[i];
        }
    }

    return WeaponSlots;
}

FWeaponSlot UInventoryComponent::GetWeaponSlot(int Slot)
{
    if (Weapons[Slot])
    {
        return *Weapons[Slot];
    }

    return FWeaponSlot();
}

bool UInventoryComponent::DropWeapon(int Slot, FWeaponSlot &DroppedWeaponSlot)
{
    if (Weapons[Slot])
    {
        DroppedWeaponSlot = *Weapons[Slot];
        auto WeaponSlot = Weapons[Slot];
        Weapons[Slot] = nullptr;
        delete WeaponSlot;
        OnWeaponDropped.Broadcast(Slot);
        return true;
    }

    return false;
}

int UInventoryComponent::GetAvailableWeaponForDrop(int Slot)
{
    for (int i = 0; i < 5; i++)
    {
        if (Weapons[i])
        {
            if (i != Slot)
            {
                return i;
            }
        }
    }

    return -1;
}
