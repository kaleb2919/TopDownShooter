// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InventoryComponent.h"
#include "GameFramework/Actor.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDS/Game/TDSGameInstance.h"
#include "PickupableItem.generated.h"

UCLASS()
class TDS_API APickupableItem : public AActor
{
    GENERATED_BODY()

public:
    APickupableItem();

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class USphereComponent* CollisionSphere = nullptr;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UStaticMeshComponent* ItemStaticMesh = nullptr;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UBillboardComponent* BillboardComponent = nullptr;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UParticleSystemComponent* ParticleSystemComponent = nullptr;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class URotatingMovementComponent* RotatingMovementComponent = nullptr;

    UPROPERTY()
    UTDSGameInstance* GameInstance;
    UPROPERTY()
    bool bIsNowDropped = false;

protected:
    virtual void BeginPlay() override;

public:
    UFUNCTION()
    void PickUp(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

    UFUNCTION()
    void PickUpEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inventory")
    void OnPickUp(UInventoryComponent* InventoryComponent, EItemType ItemType);

    UFUNCTION()
    bool PickUpAmmo(UInventoryComponent* InventoryComponent);

    UFUNCTION()
    bool PickUpWeapon(UInventoryComponent* InventoryComponent);

    UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Common)
    int AmmoCount = 20;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Common)
    EItemType Type;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Common)
    FName Name;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Common)
    USoundBase* SoundPickup = nullptr;

};
