// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDS/FuncLibrary/Types.h"
#include "InventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChanged, const FName, Name, const int, Count);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponSlotChanged, const FName, Name, const int, Slot);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponDropped, const int, Slot);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UInventoryComponent : public UActorComponent
{
public:
    UInventoryComponent();

private:
    GENERATED_BODY()

    int MaxWeapon = 5;

    TArray<FWeaponSlot*> Weapons;
    TArray<FAmmoSlot*> Items;

    FAmmoSlot* FindAmmoByName(FName AmmoName);
    FWeaponSlot* FindWeaponByName(FName WeaponName);

public:
    UPROPERTY(VisibleAnywhere, BlueprintAssignable)
    FOnAmmoChanged OnAmmoChanged;
    UPROPERTY(VisibleAnywhere, BlueprintAssignable)
    FOnWeaponSlotChanged OnWeaponSlotChanged;
    UPROPERTY(VisibleAnywhere, BlueprintAssignable)
    FOnWeaponDropped OnWeaponDropped;

    UFUNCTION(BlueprintCallable)
    bool AddAmmo(FName AmmoName, int Count, int MaxCount);

    UFUNCTION(BlueprintCallable)
    FWeaponSlot GetWeapon(const int Slot);

    UFUNCTION(BlueprintCallable)
    bool AddWeapon(FName WeaponName, const int AmmoInClip);

    UFUNCTION(BlueprintCallable)
    int GetAvailableAmmo(FName AmmoName, const int NeedCount);

    UFUNCTION(BlueprintCallable)
    int CountAmmo(FName AmmoName);

    UFUNCTION(BlueprintCallable)
    int CountWeapons();

    UFUNCTION(BlueprintCallable)
    bool IsValidWeaponSlot(int Slot);

    UFUNCTION(BlueprintCallable)
    void SetAmmoInClip(int Slot, int Count);

    UFUNCTION(BlueprintCallable)
    TArray<FAmmoSlot> GetAmmoSlots();

    UFUNCTION(BlueprintCallable)
    FAmmoSlot GetAmmoSlot(int Slot);

    UFUNCTION(BlueprintCallable)
    TArray<FWeaponSlot> GetWeaponSlots();

    UFUNCTION(BlueprintCallable)
    FWeaponSlot GetWeaponSlot(int Slot);

    UFUNCTION(BlueprintCallable)
    bool DropWeapon(int Slot, FWeaponSlot &DroppedWeaponSlot);

    UFUNCTION(BlueprintCallable)
    int GetAvailableWeaponForDrop(int Slot);
};
