#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Particles/ParticleSystemComponent.h"

#include "TDS/FuncLibrary/Types.h"
#include "ProjectileBase.generated.h"

UCLASS(Abstract)
class TDS_API AProjectileBase : public AActor
{
	GENERATED_BODY()

public:	
	AProjectileBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FProjectileParams ProjectileParams;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	
	UFUNCTION(BlueprintCallable)
	void Hit(const FHitResult& HitResult);
	UFUNCTION(BlueprintCallable)
	void ApplyDamageOnRadius(const FVector Location, const float Radius, const float Damage, const UCurveFloat* DamageFalloffCurve);
	UFUNCTION(BlueprintCallable)
	virtual void InitProjectile(const FProjectileParams& InitProjectileParams);
};
